# html-css-js

This repository is created to improve our communication and code review.

## How it works
First of all you need to clone this repository to your local computer.
There are two option:

1. Clone with SSH
2. Clone with HTTPS

I suggest you to choose the second option because it will give you more understanding of how git actually works.
After you copied the link you have to open a terminal on your computer and run the following command: `git clone copied_link`.
Then you might have to configure git globally. To do this you should run the following commands in you terminal:

`git config --global user.email=your_email@mail.com`

`git config --global user.password=your_password`

Now you are ready to start working.

Each task should be done in a new branch. 
To create a new branch run `git checkout -b feature/task_#` where `#` is number of the current task.
After you are done with the task push it to this repository using `git push origin feature/task_#` and then create the Merge Request (MR) (or it is also called Pull Request (PR)), so I will be able to review it in that way and leave you some comments.

Hope it will help you to study more efficient.

Enjoy!




